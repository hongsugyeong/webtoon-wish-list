<img src="/public/img/react.jpeg">

# 웹툰 위시 리스트

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

좋아하는 웹툰을 찜해보세요! <br>
즐기고 싶을 때 마음껏 즐기실 수 있습니다!

### 프로젝트 실행 화면
<img src="/public/img/webtoon-wish-list-list.png">
웹툰 리스트에서 마음에 드는 웹툰을 찜해주세요. <br>
찜한 웹툰은 로컬 스토리지에 저장됩니다. <br>
찜 취소도 가능하니 자유롭게 이용해 주세요!

#### 첫번째 미니 게임 : 넌 나고 난 너야 <br>
<img src="/public/img/webtoon-wish-list-wish.png">
찜한 웹툰은 해당 페이지에서 확인할 수 있습니다. <br>

### 사용한 기술
- JSON
- Local Storage
- Router