import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div>
                header
                <nav>
                    <Link to="/wish">페이지 이동</Link>
                    {/* wish 라는 폴더 이름을 지정한 거 */}
                </nav>
            </div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;