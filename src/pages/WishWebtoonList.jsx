import React, {useState} from 'react';

const WishWebtoonList = () => {
    const [wishWebtoon, setWishWebtoon] = useState(JSON.parse(localStorage.getItem('wishWebtoon')) || [])

    // 찜 버튼 누르면 위시로 바꿔주기
    const addToWish = webtoon => {
        if (wishWebtoon.find(e => e.id === webtoon.id)) {
            // 위시리스트에 들어있을 경우 버튼의 상태를 변경해줘야 됨
            setWishWebtoon(wishWebtoon.filter(e => e.id !== webtoon.id))
            // wishWebtoon중에서 webtoon이 아닌 거 setWishWebtoon에 넣어주기?
        } else {
            setWishWebtoon([...wishWebtoon, webtoon])
            // 위시리스트 추가됨
        }
    }

    return (
        <div>
            <h2>찜찜찜</h2>
            {wishWebtoon.map(webtoon => (
                <div key={webtoon.id}>
                    <h3>{webtoon.webtoonName}</h3>
                </div>
            ))}
        </div>
    )
}

export default WishWebtoonList