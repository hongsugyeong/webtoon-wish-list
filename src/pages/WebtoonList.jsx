import React, {useEffect, useState} from 'react';

const WebtoonList = () => {
    // 그냥 웹툰
    const [webtoons, setWebtoons] = useState([])
    // 사용자가 찜한 웹툰
    // 로컬 스토리지에서 wishWebtoon 가져온 뒤 JSON으로 파싱하기 실패면 빈 배열 주기
    const [wishWebtoon, setWishWebtoon] = useState(JSON.parse(localStorage.getItem('wishWebtoon')) || [])

    // 목록 불러오기
    useEffect(() => {
        fetch('http://localhost:3001/webtoons.json')
            // JSON 로컬 호스트 주소 불러오기
            .then(res => res.json())
            // JSON 형태로 바꿔주기
            .then(data => setWebtoons(data))
            // 데이터 집어넣기
    }, [])

    // 업데이트, 수정
    useEffect(() => {
        localStorage.setItem('wishWebtoon', JSON.stringify(wishWebtoon))
        // 변경된 사본으로 변경하기 위해 직렬화?
    }, [wishWebtoon])

    // 찜 버튼 누르면 위시로 바꿔주기
    const addToWish = webtoon => {
        if (wishWebtoon.find(e => e.id === webtoon.id)) {
            // 위시리스트에 들어있을 경우 버튼의 상태를 변경해줘야 됨
            setWishWebtoon(wishWebtoon.filter(e => e.id !== webtoon.id))
            // wishWebtoon중에서 webtoon이 아닌 거 setWishWebtoon에 넣어주기?
        } else {
            setWishWebtoon([...wishWebtoon, webtoon])
            // 위시리스트 추가됨
        }
    }

    // 목록 보여주기
    // 한 웹툰당 제목, 작가, 버튼이 있어야 됨
    return(
        <div>
            <h1>목록</h1>
            {webtoons.map(webtoon => (
                <div key={webtoon.id}>
                    <h2>{webtoon.webtoonName}</h2>
                    <p>{webtoon.webtoonWriter}</p>
                    <button onClick={() => addToWish(webtoon)}>
                        {wishWebtoon.find(e => e.id === webtoon.id) ? '찜 취소' : '찜 실행'}
                    </button>
                </div>
            ))}
        </div>
    )
}

export default WebtoonList;