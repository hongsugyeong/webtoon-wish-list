import './App.css';
import WebtoonList from "./pages/WebtoonList";
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import WishWebtoonList from "./pages/WishWebtoonList";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element= {<DefaultLayout><WebtoonList /></DefaultLayout>} />
            <Route path="/wish" element= {<DefaultLayout><WishWebtoonList /></DefaultLayout>} />
            {/* 자식이 레이아웃을 선택하는 방식임 */}
        </Routes>
    </div>
  );
}

export default App;
